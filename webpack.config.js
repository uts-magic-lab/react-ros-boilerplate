const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const autoprefixer = require('autoprefixer');
const path = require('path');
const {pick, defaults, mapValues} = require('lodash');
const packageInfo = require('./package');

// Default settings:
const default_env = {
    NODE_ENV: 'development',
    ROSBRIDGE_URI: 'ws://localhost:9090',
    ROS_WEB_VIDEO_URI: null, // 'http://localhost:8080',
    APP_NAME: packageInfo.displayName,
    APP_VERSION: packageInfo.version,
    PUBLIC_PATH: './'
};
// Override default settings with current environment variables:
const env = defaults(
    pick(process.env, Object.keys(default_env)),
    default_env
);
const NODE_ENV = env.NODE_ENV;

module.exports = {
    context: path.resolve(__dirname, "src"),
    entry: {
        main: [
            'react-hot-loader/patch',
            './entry.js'
        ]
    },

    output: {
        path: path.resolve(__dirname, "build"),
        publicPath: env.PUBLIC_PATH,
        filename: 'app.js'
    },

    resolve: {
        extensions: ['.jsx', '.js', '.json'],
        modules: [
            path.resolve(__dirname, "node_modules"),
            'node_modules'
        ],
        alias: {
            eventemitter2: path.resolve(__dirname, 'node_modules/eventemitter2'), // override alias in roslib "browser" config
            components: path.resolve(__dirname, "src/components"),
            lib: path.resolve(__dirname, "src/lib"),
            style: path.resolve(__dirname, "src/style")
        }
    },

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: path.resolve(__dirname, 'src'),
                enforce: 'pre',
                use: 'source-map-loader'
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                modules: false,
                                sourceMap: (NODE_ENV!=='production'),
                                importLoaders: 1
                            }
                        },
                        {
                            loader: `postcss-loader`,
                            options: {
                                sourceMap: (NODE_ENV!=='production'),
                                plugins: () => { autoprefixer() }
                            }
                        }
                    ]
                })
            },
            {
                test: /\.json$/,
                use: 'json-loader'
            },
            {
                test: /\.ya?ml$/,
                use: ['json-loader', 'yaml-loader']
            },
            {
                test: /\.(xml|html|txt|md)$/,
                use: 'raw-loader'
            },
            {
                test: /\.(svg|woff2?|ttf|eot|jpe?g|png|gif)(\?.*)?$/i,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]?[hash:8]'
                    }
                }]
            }
        ]
    },
    plugins: ([
        new webpack.NoEmitOnErrorsPlugin(),
        new ExtractTextPlugin({
            filename: 'style.css',
            allChunks: true,
            disable: NODE_ENV !== 'production'
        }),
        new webpack.DefinePlugin({
            'process.env': mapValues(env, JSON.stringify)
        }),
        new HtmlWebpackPlugin({
            template: './index.html.ejs',
            inject: false,
            minify: { collapseWhitespace: false },
            packageInfo: packageInfo
        }),
        new CopyWebpackPlugin([
            { from: './assets', to: './' }
        ])
    ]).concat(NODE_ENV==='production' ? [
        new webpack.optimize.UglifyJsPlugin({
            output: {
                comments: false
            },
            compress: {
                unsafe_comps: true,
                properties: true,
                keep_fargs: false,
                pure_getters: true,
                collapse_vars: true,
                unsafe: true,
                warnings: false,
                screw_ie8: true,
                sequences: true,
                dead_code: true,
                drop_debugger: true,
                comparisons: true,
                conditionals: true,
                evaluate: true,
                booleans: true,
                loops: true,
                unused: true,
                hoist_funs: true,
                if_return: true,
                join_vars: true,
                cascade: true,
                drop_console: true
            }
        })
    ] : [
        new webpack.NamedModulesPlugin()
    ]),

    stats: { colors: true },

    node: {
        global: true,
        process: false,
        Buffer: false,
        __filename: false,
        __dirname: false,
        setImmediate: false
    },

    devtool: NODE_ENV==='production' ? 'source-map' : 'cheap-module-eval-source-map',

    devServer: {
        port: process.env.PORT || 8080,
        host: 'localhost',
        publicPath: env.PUBLIC_PATH == './' ? '/' : env.PUBLIC_PATH,
        contentBase: './src',
        historyApiFallback: true,
        open: true,
        openPage: '',
        proxy: {
            // OPTIONAL: proxy configuration:
            // '/optional-prefix/**': { // path pattern to rewrite
            //   target: 'http://target-host.com',
            //   pathRewrite: path => path.replace(/^\/[^\/]+\//, '')   // strip first path segment
            // }
        }
    }
};
