import ROSLIB from 'roslib';
import {toast} from 'react-toastify';

// Declare the main ros connection singleton.
// It will not actually connect until you run ros.connect(process.env.ROSBRIDGE_URI);
let ros = new ROSLIB.Ros();

ros._connectAttempts = 1;
ros._connectSuccesses = 0;

ros.on('connection', ()=>{
    ros._connectSuccesses++;
    console.log(`Connected to Rosbridge ${ros.socket.url}`);
    // toast.success('Connected to ROS', {
    //     closeButton: false,
    //     autoClose: 500
    // });
});

ros.on('close', ()=>{
    if (ros._connectSuccesses > 0) {
        toast.error('ROS Connection closed');
    }
    else {
        toast.warn(`Could not connect to ROS ${ros.socket.url}`);
    }
});

ros.on('error', (event)=>{
    // prevent printing duplicate error to console
});

export {ros, ROSLIB};
export default ros;
