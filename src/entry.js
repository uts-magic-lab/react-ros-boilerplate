import 'core-js/modules/es6.object.assign';
import 'whatwg-fetch';

import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import App from './components/app';
import 'style/main.css';

function render(Component) {
    ReactDOM.render(
        <AppContainer>
            <Component/>
        </AppContainer>,
        document.getElementById('app')
    );
}

try {
    render(App);
}
catch (error) {
    // Display initial errors onscreen to assist debugging tablets
    let statusDisplay = document.getElementById('status-display');
    statusDisplay.setAttribute('data-status', 'Error');
    statusDisplay.innerText = error.stack;
}

// Hot Module Replacement API
if (module.hot) {
    module.hot.accept('./components/app/index.js', () => {
        let App = require('./components/app').default;
        render(App);
    });
}
