import React from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { ToastContainer } from 'react-toastify';
import ros from 'lib/ros';
import 'react-toastify/dist/ReactToastify.min.css';
import './style.css';

import Home from 'components/home';
import NotFound from './not-found';

// This component handles routing and notifications.
// To add a new page, import it here and declare its <Route> in the <Switch> below.

export default class App extends React.Component {
    constructor(props) {
        super(props);
        const history = createHistory({
            basename: process.env.PUBLIC_PATH === './' ? document.location.pathname : process.env.PUBLIC_PATH
        });
        this.state = {
            history: history,
            lastPageTime: 0,
            pageTime: Date.now()
        };
        this.handleNavigation = (location, action)=>{
            // ensure location.state.ts is the first time that this location appears in history
            if (!(location.state && location.state.ts)) {
                const newState = Object.assign({}, location.state, {ts: Date.now()});
                this.state.history.replace(location.pathname, newState);
                return; // this handler will run again immediately with the new state
            }
            this.setState({
                lastPageTime: this.state.pageTime,
                pageTime: location.state.ts
            });

            // record the page view in the access log
            const analyticsUrl = `events?ts=${Date.now()}&viewPage=${encodeURIComponent(location.pathname)}`;
            fetch(analyticsUrl);
        };
    }

    componentWillMount() {
        this.handleNavigation(this.state.history.location, 'PUSH');
        this.state.history.listen(this.handleNavigation);

        ros.connect(process.env.ROSBRIDGE_URI);
    }

    render() {
        // setting forward/back based on timestamp will work in most situations.
        // watch out for: reloading and then going forward, going back and forward before animation completes
        let navDirection = 'forward';
        if (this.state.pageTime < this.state.lastPageTime) {
            navDirection = 'back';
        }
        return (
            <div className="app">
                <Router history={this.state.history}>
                    <Route render={({ location }) => (
                        <TransitionGroup component={'main'} className={"router-nav-"+navDirection}>
                            <CSSTransition key={location.key} classNames="transition-vertical" timeout={500}>
                                <Switch location={{pathname: location.pathname}}>
                                    <Route exact path="/" component={Home} />
                                    <Route component={NotFound} />
                                </Switch>
                            </CSSTransition>
                        </TransitionGroup>
                    )}/>
                </Router>
                <ToastContainer
                    type="deafult"
                    position="bottom-center"
                    autoClose={false}
                    hideProgressBar={true}
                    newestOnTop={true}
                    closeOnClick
                    pauseOnHover
                />
            </div>
        );
    }
}
