import React from 'react';
import { Link } from 'react-router-dom';

export default class NotFound extends React.Component {
    render() {
        return (
            <div>
                <p><tt>{this.props.location.pathname}</tt> not found</p>
                <p><Link to="/">Go home</Link></p>
                <p><button onClick={()=>{this.props.history.goBack();}}>Go back</button></p>
            </div>
        );
    }
}
