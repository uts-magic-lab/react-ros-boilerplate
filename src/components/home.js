import React from 'react';

export default class Home extends React.Component {
    render() {
        return (
            <div>
                <h1>{process.env.APP_NAME}</h1>
                <p>Edit <tt>src/components/home.js</tt> to change this page.</p>
                <p>Edit <tt>src/components/app/index.js</tt> to add routes to other pages.</p>
            </div>
        );
    }
}
