# React ROS Boilerplate

Template for robot web applications.

### Getting Started

To start a new app:

1. Download this project under a new name:

    ```shell
    git clone https://gitlab.com/uts-magic-lab/react-ros-boilerplate.git $YOUR_APP_NAME
    cd $YOUR_APP_NAME
    git remote rm origin
    ```

2. Edit `package.json` to set your app's name and description. Set `name` to an all-lowercase name with no spaces, and set `appName` and `description` to human-readable text for display. These values will be used in the generated `index.html`.

3. Edit `webpack.config.js` to set default environment variables such as the Rosbridge IP address.

4. Edit `README.md` and remove the **Getting Started** section.

### Installation

1. Install [Node.js](https://nodejs.org/en/download/) on your system.

2. Download dependencies by running this in the project directory:

    ```shell
    npm install
    ```

### Development

Source code is in `src/`. Files in `src/assets/` will be copied to `build/`. Files imported by `src/entry.js` will be bundled into `build/app.js` and `build/style.css`.

Project scripts are defined in `package.json` and can be executed with `npm run`:

- **`npm run build`**: Use [webpack](https://webpack.github.io/docs/) to compile a static site into the `build/` folder.

- **`npm run start`**: Build the app and [serve](https://www.npmjs.com/package/serve) it at [http://localhost:5000/](http://localhost:5000/).

- **`npm run dev`**: Use [webpack dev server](https://webpack.github.io/docs/webpack-dev-server.html) to serve the app at [http://localhost:8080/](http://localhost:8080/) and recompile every time you change a source file. Some components can be recompiled without losing state, using [hot module replacement](https://webpack.js.org/concepts/hot-module-replacement/).

- **`npm run lint`**: Check code style.

- **`npm run test`**: Run automated tests.

### Deployment

Run `npm run build` and copy the `build/` folder to your web server. `.gitlab-ci.yml` has an example of doing this automatically for GitLab Pages.

